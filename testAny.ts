function testAny (input:any){
  console.log(input.length);
}

console.log("Test Any");
testAny("jan");
testAny(56);

function testUnknown (input:unknown){
  if(typeof  input ==="string") {
    console.log(input.length);
  } else{
    console.log("Not a string: " + input)
  }
}
console.log("Test Unknown");

testUnknown("zeg een iets");
testUnknown(89);
const tuesday = "day";